This is an Arduino 1.0 compatiable sketch
-----------------------------------------

Programming steps:

Download and Install Arduino software: http://arduino.cc/en/Main/Software

Unzip libraries.zip. The /libraries directory contains all libraries in my local system. A subset of them are required to run UNDERCURRENT_FRIDGE.ino. They can all be coppied into the 'Arduino/libraries' directory of the application (libraries directory should be created if it doesn't exist) . In the Mac environment, this is: /Users/myuser/Documents/Arduino

Open UNDERCURRENT_FRIDGE.ino in Arduino IDE.

Go to "Credentials.h" tab and apply correct WIFI user/password.

Connect computer to Undercurrent Fridge via USB.

In Arduino IDE, select Tools > Board > Arduino Uno.  select Tools > Serial Port > [something]USB

File > Upload
