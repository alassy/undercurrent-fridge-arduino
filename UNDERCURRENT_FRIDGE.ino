#include <avr/pgmspace.h>

#include <avrpins.h>
#include <max3421e.h>
#include <usbhost.h>
#include <usb_ch9.h>
#include <Usb.h>
#include <usbhub.h>
#include <avr/pgmspace.h>
#include <address.h>
#include <hidboot.h>

#include <printhex.h>
#include <message.h>
#include <hexdump.h>
#include <parsetools.h>

#include <Servo.h> 
#include <Bounce.h>

#include <SPI.h>
#include <WiFly.h>
#include "Credentials.h"

//DEFINITIONS
#define SSUSB 6 //cable select pin for WAV shield
#define SSWIFLY 10 //cable select pin for WIFLY shield

#define BUTTON 3

#define LED1 A1
#define LED2 A2
#define LED3 A3

#define SERVO_PIN 2

#define STATE_SEND 1
#define STATE_RECEIVE 2
#define STATE_READ_UPC 3
#define STATE_CHECK_USB 4
#define STATE_DEFAULT 5

#define UPC_STATE_WAIT 0
#define UPC_STATE_FOOD_OUT 1
#define UPC_STATE_FOOD_IN 2

#define LOCK_OPEN 65
#define LOCK_CLOSED 93

//byte server[] = { 173, 203, 94, 85 }; // undercurrent
byte server[] = { 23, 21, 227, 160 }; // adamlassy.com

String DOMAIN = "fridge/fridge.php";
//String DOMAIN = "fridge.php";

int UPC_LENGTH = 12;
int upc_count = 0;
String upc = "";

int UPC_STATE = LOW;
int lastButtonVal = 0;
int usb_status = 0;

int scale_count = 0;
boolean scale_init = false;

long TIME_LAST_KEY = 0;
boolean getting_number = false;

String sResponse = "";

int buttonVal;

Servo myservo;  // create servo object to control a servo 

int SERVO_STATE = LOCK_OPEN;
int STATE_COUNT = 0;
int receive_count = 0;

boolean init_receive_count = false;
boolean READING = false;
int FOOD_BLINK_STATE;

WiFlyClient client(server, 80);

Bounce bouncer = Bounce( BUTTON,5);

int current_state;

void enableWiFlyShield()
{
  digitalWrite(SSWIFLY, LOW);
  digitalWrite(SSUSB, HIGH);
}

//this enables the cable selection pin for WAV shield
void enableUSBShield()
{
  digitalWrite(SSWIFLY, HIGH);
  digitalWrite(SSUSB, LOW);
}

/*
  WIFLY FUNCTION
*/

int sendUpc(String upc)
{
  digitalWrite(SSWIFLY, LOW);
  
  upc = (String)upc;
  //sendCommand("fridge.php?action=upc&val=9876&in=" + (String)UPC_STATE);
  sendCommand(DOMAIN + "?action=upc&val=" + upc + "&in=" + (String)UPC_STATE);
  digitalWrite(SSWIFLY, HIGH);
}

int sendCommand(String target)
{
  Serial.println("I'm Connecting ...");
  if (client.connect()) {
    Serial.println("Sending ...");
    Serial.println(target);
    client.print("GET /");
    client.print(target);
    client.println(" HTTP/1.0");
    client.println();
  } else {
    Serial.println("connection failed");
  }
}


/*
  KEYBOARD FUNCTIONS
  */
class KbdRptParser : public KeyboardReportParser
{
        void PrintKey(uint8_t mod, uint8_t key);
        
protected:
	virtual void OnKeyDown	(uint8_t mod, uint8_t key);
	virtual void OnKeyUp	(uint8_t mod, uint8_t key);
	virtual void OnKeyPressed(uint8_t key);
};

void KbdRptParser::PrintKey(uint8_t m, uint8_t key)	
{
    MODIFIERKEYS mod;
    *((uint8_t*)&mod) = m;
    Serial.print((mod.bmLeftCtrl   == 1) ? "C" : " ");
    Serial.print((mod.bmLeftShift  == 1) ? "S" : " ");
    Serial.print((mod.bmLeftAlt    == 1) ? "A" : " ");
    Serial.print((mod.bmLeftGUI    == 1) ? "G" : " ");
    
    PrintHex<uint8_t>(key);

    Serial.print((mod.bmRightCtrl   == 1) ? "C" : " ");
    Serial.print((mod.bmRightShift  == 1) ? "S" : " ");
    Serial.print((mod.bmRightAlt    == 1) ? "A" : " ");
    Serial.println((mod.bmRightGUI    == 1) ? "G" : " ");
};

void KbdRptParser::OnKeyDown(uint8_t mod, uint8_t key)	
{
    //Serial.println("Key Down");
    //Serial.print("DN ");
    //PrintKey(mod, key);
    uint8_t c = OemToAscii(mod, key);
    
    if (c)
        OnKeyPressed(c);
}

void KbdRptParser::OnKeyUp(uint8_t mod, uint8_t key)	
{
    //Serial.print("UP ");
    //PrintKey(mod, key);
}


void KbdRptParser::OnKeyPressed(uint8_t key)	
{
    
    Serial.println ((char)key);
    
    Serial.print(">");
    Serial.println(key);
    
    if (key >= 48 && key < 57)
    {upc += (char)key;}
    
    TIME_LAST_KEY = millis();
    setState(STATE_READ_UPC);  
};

USB     Usb;
//USBHub     Hub(&Usb);
HIDBoot<HID_PROTOCOL_KEYBOARD>    Keyboard(&Usb);

uint32_t next_time;

KbdRptParser Prs;

void setup()
{
    Serial.begin( 115200 );

    pinMode(SERVO_PIN, OUTPUT);

    myservo.attach(SERVO_PIN);
    
    myservo.write(LOCK_OPEN);

    Serial.println("START USB ..."); 
    if (Usb.Init() == -1)
        Serial.println("OSC did not start.");
      
    delay( 200 );
  
    pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
    pinMode(LED3, OUTPUT);
    
    next_time = millis() + 5000;
  
    Keyboard.SetReportParser(0, (HIDReportParser*)&Prs);

    Serial.println("START WIFLY ...");    
    WiFly.begin();
    Serial.println("WIFLY BEGIN");

    if (!WiFly.join(ssid,passphrase)) {
      Serial.println("Association failed.");
      while (1) {
        // Hang on failure.
        
        digitalWrite(LED1, HIGH);
        digitalWrite(LED2, HIGH);
        digitalWrite(LED3, HIGH);
      }
    }  
    Serial.println("WIFLY JOINED");
  
    pinMode(SSWIFLY, OUTPUT);
    pinMode(SSUSB, OUTPUT);
    pinMode(BUTTON, INPUT);
        
    digitalWrite(SSWIFLY, HIGH);
    digitalWrite(SSUSB, HIGH); 

    blink(LED1, 2);
    blink(LED2, 2);
    blink(LED3, 2);
        
    scale_init = true;
 
    setState(STATE_DEFAULT);
}

void advance_upc_state()
{
  UPC_STATE++;

  if (UPC_STATE > 2)
  {
    UPC_STATE = 0; 
  }
  
  if (UPC_STATE != 0)
  {   
    TIME_LAST_KEY = millis();
  }
}

void display_upc_state()
{
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);

  
  switch (UPC_STATE)
  {
    case UPC_STATE_WAIT:

      digitalWrite(LED3, HIGH);
      break;
    case UPC_STATE_FOOD_OUT:

      digitalWrite(LED1, HIGH);
      break;
    case UPC_STATE_FOOD_IN:
      digitalWrite(LED1, HIGH);
      digitalWrite(LED3, HIGH);
      break;
  }  
}

void readButton()
{
  /*
    READ BUTTON
  */
  if (bouncer.update() && bouncer.read() == HIGH)
  {
    advance_upc_state();
  }

  display_upc_state();
}

void checkUSB()
{

   digitalWrite(SSUSB, LOW);
   usb_status = Usb.Task();
   digitalWrite(SSUSB, HIGH);
}

void readUSB()
{
    //Serial.println(usb_status);
    
    //Serial.print("-");
    //Serial.println(millis() - TIME_LAST_KEY);
    
    if (upc.length() > 0 && (millis() - TIME_LAST_KEY) > 30)
    {
      digitalWrite(LED2, HIGH);
      scale_count = 0;
      
      Serial.println("SENDING UPC");
      
      digitalWrite(SSWIFLY, LOW);
      sendUpc(upc);
      upc = ""; 
      Serial.println("SENT UPC");
      digitalWrite(SSWIFLY, HIGH);
      digitalWrite(LED2, LOW);
      
      setState(STATE_DEFAULT);
    }  
}

void sendWeight()
{
      Serial.print("SEND WEIGHT: ");
      //sResponse = "";

      int val = analogRead(0);    // read the input pin
      Serial.println(val); 
 
      digitalWrite(LED2, HIGH);
      sendCommand(DOMAIN + "?action=milk&val=" + (String)val);
      digitalWrite(LED2, LOW);
       
      Serial.println("SENT WEIGHT"); 
}

void talk()
{
      digitalWrite(SSWIFLY, LOW);
      sendWeight();
        
      setState(STATE_RECEIVE);
      receive_count = 0;
      init_receive_count = false;
      digitalWrite(SSWIFLY, HIGH);  
}

void receiveWeight()
{  
    //RECEIVE WEIGHT  
  if (client.available()) {

    init_receive_count = true;
    receive_count = 0;
    
    char c = client.read();

 Serial.print(c);
 
      //Serial.println("client avail");
      if (c != ' ') {
        sResponse += c;
      }

      if (c == '\n') {
        //Serial.println(sResponse);
        //READING = false;
        checkResponse();
        sResponse = "";
        //Serial.print(sResponse);
      }
    
  } else {
    
    //Serial.print("-");
    if ( init_receive_count )
    {
  
      receive_count++;
      if (receive_count > 5000)
      {
        //Serial.println(sResponse);
        Serial.println("TRY AGAIN");

        setState(STATE_DEFAULT);
        receive_count = 0;
      }
      
    }
  }
}
  
void checkResponse()
{
  //Serial.println("CHECK RESPONSE");
  //Serial.println(sResponse);
  
  if (sResponse.indexOf("lock0") >= 0 || sResponse.indexOf("lock1") >= 0)
  {
    Serial.println("HIT");
    Serial.println(sResponse);
    //Serial.println(sResponse);

    if (sResponse.indexOf("lock0") >= 0)
    {SERVO_STATE = LOCK_OPEN;Serial.println("* OPEN");} 
    
    if (sResponse.indexOf("lock1") >= 0) 
    {SERVO_STATE = LOCK_CLOSED;Serial.println("* CLOSE");}
  
    //Serial.println(sResponse);
    sResponse = "";
    
    setState(STATE_DEFAULT);
    receive_count = 0;
  }

}

void setState(int iState)
{
  if (iState != current_state)
  {
    Serial.print("* STATE : ");
    Serial.println(iState);
    
    current_state = iState;
    STATE_COUNT = 0;

  }
}

void blink(int LEDi, int n)
{
   digitalWrite(LEDi, LOW);
   delay(300);
   for (int i=0; i<n; i++)
   {
      digitalWrite(LEDi, HIGH);
      delay(300);
      digitalWrite(LEDi, LOW);
      delay(300);
   }
}

void loop()
{  
  //READ BUTTON
  readButton();
  
  //WRITE SERVO
  myservo.write(SERVO_STATE);
  
  //Serial.println("Check Wifi");
  
  switch (current_state) {

    case STATE_READ_UPC:

      checkUSB();
      readUSB();
      break;
 
    case STATE_RECEIVE:
     
      //Serial.println("RECEIVE");      
      digitalWrite(SSWIFLY, LOW);
      //RECEIVE WEIGHT
      receiveWeight();
      digitalWrite(SSWIFLY, HIGH);
      //Serial.println(receive_count);
      break;
      
    default:

      if (UPC_STATE == UPC_STATE_WAIT)
      {
        delay(1000);
        talk();
      }
      else
      {
        checkUSB();
        
        if ((millis() - TIME_LAST_KEY) > 30000) {
          UPC_STATE = UPC_STATE_WAIT;
        }
      }
      break;
  }
  
  STATE_COUNT++;  
}

